<?php
require_once __DIR__ . '/init.php';
require_once __DIR__ . '/connectMysql.php';

use JamesGordo\CSV\Parser;
use models\User;

// Initalize the Parser
$inviters = new Parser(__DIR__ . '/for_parse.csv');

$all = $inviters->all();

$count = count($all);

$collectUserData = [];
$i = 0;
$specialCount = 0;

// loop through each user and echo the details
foreach($all as $user) {

    if (isset($user)) {
        if(null === $user->users || $specialCount === 6) {
            $i++;
            $specialCount = 0;
        } else {
            $collectUserData[$i][] = $user->users;
            $specialCount++;

            if($specialCount === 6) {
                $newUser = new User();

                $newUser->setName(str_replace("ФИО:", "", $collectUserData[$i][0] ?? ''));
                $newUser->setWork(str_replace("Работает:", "", $collectUserData[$i][1] ?? ''));
                $newUser->setPhone(str_replace("Телефон:", "", $collectUserData[$i][2] ?? ''));
                $newUser->setEmail(str_replace("Email:", "", $collectUserData[$i][3] ?? ''));
                $newUser->setCity(str_replace("город:", "", $collectUserData[$i][4] ?? ''));
                $newUser->setOrganization(str_replace("Организация:", "", $collectUserData[$i][5] ?? ''));

                $collectUserData[$i] = $newUser->getData();
            }
        }

    }
}



var_dump($collectUserData);
