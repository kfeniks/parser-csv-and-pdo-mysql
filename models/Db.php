<?php

namespace models;

use SimpleCrud\Database;

class Db
{

    public static function connect(string $dsn, string $username, string $password): Database
    {

        $pdo = new \PDO($dsn, $username, $password);

        $db = new Database($pdo);

        return $db;
    }

}