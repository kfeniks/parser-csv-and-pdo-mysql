<?php

namespace models;

class User
{

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $work;

    /**
     * @var string
     */
    public $phone;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $city;

    /**
     * @var string
     */
    public $organization;

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = trim($name);
    }

    /**
     * @param string $work
     */
    public function setWork($work)
    {
        $this->work = trim($work);
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = trim($phone);
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = trim($email);
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = trim($city);
    }

    /**
     * @param string $organization
     */
    public function setOrganization($organization)
    {
        $this->organization = trim($organization);
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return [
            'fio' => $this->name,
            'work' => $this->work,
            'phone' => $this->phone,
            'email' => $this->email,
            'city' => $this->city,
            'org' => $this->organization,
        ];
    }


}